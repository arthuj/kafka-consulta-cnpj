package br.com.itau.biro.log;

import br.com.itau.biro.producer.Empresa;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

@Component
public class EmpresaConsumer {

    @KafkaListener(topics = "spec3-arthur-jorge-1", groupId = "log-1")
    public void escreverLog(@Payload Empresa empresa) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        Writer writer = new FileWriter("log.csv",true);
        StatefulBeanToCsv<Empresa> beanToCsv = new StatefulBeanToCsvBuilder(writer).build();

        beanToCsv.write(empresa);

        System.out.println("Log salvo");
        writer.flush();
        writer.close();

    }
}
