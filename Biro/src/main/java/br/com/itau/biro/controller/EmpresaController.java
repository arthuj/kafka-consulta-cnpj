package br.com.itau.biro.controller;

import br.com.itau.biro.producer.Empresa;
import br.com.itau.biro.producer.EmpresaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {

    @Autowired
    EmpresaProducer empresaProducer;

    @PostMapping("/{cnpj}")
    public Empresa criarEmpresa(@PathVariable String cnpj){
       return empresaProducer.enviaorAoKafka(cnpj);
    }
}
