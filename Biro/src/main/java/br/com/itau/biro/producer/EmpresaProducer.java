package br.com.itau.biro.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EmpresaProducer {

    @Autowired
    KafkaTemplate<String,Empresa> producer;

    public Empresa enviaorAoKafka(String cnpj){
        Empresa empresa = new Empresa();
        empresa.setCnpj(cnpj);
        producer.send("spec3-arthur-jorge-2", empresa);

        return empresa;
    }

}
