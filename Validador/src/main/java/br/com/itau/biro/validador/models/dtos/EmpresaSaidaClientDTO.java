package br.com.itau.biro.validador.models.dtos;

public class EmpresaSaidaClientDTO {
    private String nome;
    private String capital_social;

    public EmpresaSaidaClientDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCapital_social() {
        return capital_social;
    }

    public void setCapital_social(String capital_social) {
        this.capital_social = capital_social;
    }
}
