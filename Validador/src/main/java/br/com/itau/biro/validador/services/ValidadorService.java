package br.com.itau.biro.validador.services;

import br.com.itau.biro.producer.Empresa;
import br.com.itau.biro.validador.clients.ReceitaClient;
import br.com.itau.biro.validador.models.dtos.EmpresaSaidaClientDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class ValidadorService {


    @Autowired
    ReceitaClient receitaClient;

    @Autowired
    KafkaTemplate<String, Empresa> kafkaTemplate;

    public void validaCNPJ(String cnpj) {
        final BigDecimal validarReceita = new BigDecimal(1000000);

        EmpresaSaidaClientDTO empresaSaidaClientDTO = receitaClient.getPorCNPJ(cnpj);

        Empresa empresa = new Empresa();
        empresa.setCnpj(cnpj);
        empresa.setCapitalSocial(empresaSaidaClientDTO.getCapital_social());
        empresa.setNome(empresaSaidaClientDTO.getNome());

        if (new BigDecimal(empresa.getCapitalSocial()).compareTo(validarReceita) > 0) {
            System.out.println("Empresa " + empresa.getNome() + " tem receita maior que um milhão");
            kafkaTemplate.send("spec3-arthur-jorge-1", empresa);
        }
        else {
            System.out.println("Empresa " + empresa.getNome() + " tem receita menor que um milhão");
        }
    }

}
