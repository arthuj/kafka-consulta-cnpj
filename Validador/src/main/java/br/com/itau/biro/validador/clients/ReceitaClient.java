package br.com.itau.biro.validador.clients;

import br.com.itau.biro.validador.models.dtos.EmpresaSaidaClientDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "empresa", url = "https://www.receitaws.com.br/")
public interface ReceitaClient {

    @GetMapping("/v1/cnpj/{cnpj}")
    EmpresaSaidaClientDTO getPorCNPJ(@PathVariable String cnpj);

}
