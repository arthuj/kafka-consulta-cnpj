package br.com.itau.biro.validador.consumers;

import br.com.itau.biro.producer.Empresa;
import br.com.itau.biro.validador.services.ValidadorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class ValidadorConsumer {
    @Autowired
    ValidadorService validadorService;

    @KafkaListener(topics = "spec3-arthur-jorge-2", groupId = "teste-1")
    public void validar(@Payload Empresa empresa)
    {
        System.out.printf("Recebi o CNPJ: "+ empresa.getCnpj() + "\n");
        validadorService.validaCNPJ(empresa.getCnpj());
    }
}
